# INTRODUCTION
## Getting to Know Me

![gambaq__2___1_](/uploads/4eba5e786d0c0cfd37d353872320644f/gambaq__2___1_.jpg)


- Hi, my name is Nuratikah Binti Abu Zaki or known as **Atikah Zaki**

- 22 years old

- I'm from Kajang, Selangor

- These are my strength and weaknesses:

| strength | weakness |
| ------ | ------ |
| creativity | overthinker |
| focused |  |

- ~~I love sleeping. :sleeping:~~ I mean, I love reading. :smiley: :book:

>Nice to meet you ! :raised_hand:
