## Engineering Logbook Week 02

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 4 November 2021 |


**1. What is our agenda this week and what are our/my goals?**

There was a discussion regarding the project milestones with the mixed group and subsystem. The discussion includes thinking about the tasks that we are expecting to do together with the duration of completion of the tasks during these 14 weeks of doing this project. Our goal is to complete the Gantt Chart that will include all of the discussion that we did regarding the project milestones, tasks and duration of completion. 

**2. What discussion did you/your team make in solving a problem?**

Each of us gave opinions on which parts are needed to be redesign or improve. Then, after listing down those opinions, we have decided that there are 2 main part that needed to be considered for designing which are the gondola and thruster arm. Since there are 9 people in the Design/ Structure group, we decided to split into two groups that which are gondola team and thruster arm team for efficiency.  

**3. How did you/your team make those decisions (method)**

Since we have a big group, it is better to split into two smaller groups in order to focus on the problem faster and our work will become more efficient while still keeping track of time. 

**4. Why did you/your team make that choice (justification) when solving the problem?**

Mostly because of time and efficiency. Time plays a big part in our project milestones since we have only 14 weeks to complete the project. By having two smaller groups, we can focus on the design of both parts at the same time in a week rather than focusing on just one part. 

**5. What was the impact on you/your team/the project when you make that decision?**

It was easier for us to discuss on the two main design parts ideas and there was no overlapped voices during discussion.

**6. What is the next step?**

Start giving ideas on the overall design of gondola and thruster arm. Also, picking the suitable materials needed for the two main designs. 
