## Engineering Logbook Week 06

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 3 December 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Met up with Fiqri at Lab H2.1(IDP Lab) again on Wednesday & Thursday
- Discuss on the improvements for the gondola design
- Discuss on the payload system attachment to the gondola

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Syahirah |
| Process monitor | Najwa |
| Recorder | Atikah | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- There was a discussion on to make the gondola having two layers only and that it will be opened from the middle with a latch
- The carrier board will be inverted so that it is easier for maintenance as well
- Payload attachment discussions, there was two ideas, firstly being the slot method and second being the clamping method


**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- The gondola would be easier to open and a faster way to access the carrier board, esc and battery
- Since the payload team tank holder will be using a pvc pipe, that is why we have discussed on making a slot or a clamp, we have decided to use clamp 


**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- It will look better aesthetically and also the functionality, would be optimized
- Below are the new design with a latch on each sides
- The clamp would be underneath the gondola on the front and back sides

![photo_2021-12-03_14-28-44](/uploads/80ebca5aaa1af56518bc3e8c01583b9c/photo_2021-12-03_14-28-44.jpg)
top layer
![photo_2021-12-03_14-28-40](/uploads/8add78d1df73eb2c5c40f63cb63a09bc/photo_2021-12-03_14-28-40.jpg)
bottom layer
![photo_2021-12-03_14-28-36](/uploads/0221b5235486291a0fbdcace81acd18d/photo_2021-12-03_14-28-36.jpg)
assembled gondola


**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Makes the gondola look better overall and easy access for carrier board
- The clamp would make it easier to attach/detach the payload system to the gondola

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Brush up on the gondola design and also start to 3D print the gondola
