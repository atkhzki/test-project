## Engineering Logbook Week 12

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 13 January 2022 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Start to laser cut the parts and then assembling it
- Brainstorm other type of glue instead of using hot glue since has a chance of melting during flight

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Syahirah |
| Process monitor | Najwa |
| Recorder | Atikah | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Decided to use epoxy glue to assemble the gondola

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- Epoxy glue are less likely to melt during the flight test on a sunny day

**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- Epoxy glue has a high corrosion resistance

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Stronger gondola structure after assembly
- This is the gondola after assembly:

![latest_gondola__1_](/uploads/cf8c9ec34285f5ace090cffb8e79d9ec/latest_gondola__1_.jpg)

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Apply putty spray on surface of the gondola
- Spray paint the gondola in black
- Install latch for the gondola
