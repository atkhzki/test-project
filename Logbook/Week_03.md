## Engineering Logbook Week 03

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 11 November 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Meeting with Fiqri on Google Meet regarding the thruster arm and gondola design on Wednesday. 
- Fiqri told us about the issues that the previous project had with the thruster arm and gondola
- Gondola needed a completely new design while the thruster arm design just needed an improvement
- The gondola team discussed about the material needed for the gondola structure, some of the suggestions are balsa wood and 3D print ABS filament. The final material has not been decided yet. 
- Presentation of gantt chart for the subsystem by Amir on Friday.

**Team: Group 5**
- Discussed and appointed a coordinator, checker, process monitor and recorder among our group members for the week. 
- People in charge of these roles are:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Lee |
| Process monitor | Najme |
| Recorder | Thebban| 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Brainstorming ideas on which material is the suitable one.
- Sketched ideas on the gondola design as a whole (3 layered design containing ESC, carrier board and battery).
- Any ideas were shared in the chatroom for Gondola Team

**Team: Group 5**
- Volunteers were given the roles that they wanted 
- Ina will be in charge of rotating the roles every week.

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- By sharing our ideas in the chatroom, we can pick the best one amongst them.

**Team: Group 5**
- It is easier for the coordinator to rotate it so that it will be fair for everyone 


**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- To share ideas easier and get a fast response from the group member instead of making a virtual meeting each time when discussing

**Team: Group 5**
- Fair for everyone and so that everyone will get a chance being in a charge for each role.

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Easy communication and fast feedback from gondola team members on the shared idea of design and material.

**Team: Group 5**
- Everyone can agree and accept on the roles that they will be given in the upcoming weeks

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Meet up with Fiqri again on week 4 at the lab H2.1 to discuss about our ideas
- Draw a sketch to visualize our idea easily

**Team: Group 5**
- Discuss each subsystems progress and also complete the week 3 engineeering logbook.
