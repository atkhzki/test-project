## Engineering Logbook Week 11

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 6 January 2022 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Scale down the gondola dimensions 
- After measuring, changes are made to the SolidWorks file
- Brainstorm on how to keep the ESC outside the gondola without looking messy

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Thebban |
| Process monitor | Jay |
| Recorder | Najwa | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Decide to scale it down as small as possible while still making sure that the wires can fit in the gondola
- Decide to make an ESC sleeve on top part of gondola

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- To reduce weight of gondola and also since the gondola does not have to support the weight of the payload anymore
- ESC sleeve is added so that it will stay in place during the test flight and there will be minimum to no vibration of the ESC

**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- All new measurements are confirmed after checking the tank size and by thinking how much of the original gondola dimensions can be reduced as seen in the picture below:

![scaling_down](/uploads/d3f84438ed68bd59d49a5b3ec686ec13/scaling_down.jpeg)

- To ensure the protection of the ESC components while in flight

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- The latest gondola can has its weight reduced
- The ESC sleeve will keep the ESC safe


**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Assemble the gondola
