## Engineering Logbook Week 13

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 27 January 2022 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Applying method of attachment to the gondola
- Attaching the gondola to the airship belly

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Atikah |
| Process monitor | Thebban |
| Recorder | Najme | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- using velcro and strings to attach the gondola to the airship

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- The applied velcro looks like:

![applied_velcro](/uploads/0eb296959e5001dc7e09cdaea9fbf80b/applied_velcro.jpeg)

- This is how the gondola looks like attached with all of the components: 

![attached_gondola](/uploads/f05915fb36d3c57e3b51c1e27c55c621/attached_gondola.jpg)

**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- So that the gondola will have less to no movement during the flight test

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- The gondola is safely secured under the airship belly during the flight test

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Flight test
