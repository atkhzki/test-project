## Engineering Logbook Week 08

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 16 December 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Start to 3D print the gondola
- Need to rethink of another method instead of 3D printing because of the time constraint

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Atikah |
| Process monitor | Thebban |
| Recorder | Najme | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- The material of gondola needs to be changed to plywood
- Then, the plywood is cut using laser cutter and attached using hot glue gun

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- The 3D printing takes up to 4 days to complete so time is limited since it was scheduled that the airship will have its first flight test on w8's Friday.


**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- Gondola was designed using Solidworks as usual and it is transferred to another software called RD works.
- Laser cutting process is as below:

![Laser_cutting_process](/uploads/23d65c0095154bec0939821556e65441/Laser_cutting_process.png)
- The complete assembled gondola is as below:

![assemble_without_latch](/uploads/6249f37021740634d5c82d00293fe77c/assemble_without_latch.jpeg)

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- The assembly of the gondola can be reduced greatly in terms of time and weight which were the major concerns for the flight test
- The weight is reduced from 3.5kg (3D printing) to 1.9kg (plywood)

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Assemble the latch on each side of the gondola and put all of the control system component in the gondola to test it out.
