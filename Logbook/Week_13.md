## Engineering Logbook Week 13

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 20 January 2022 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Apply putty spray on the gondola surface
- Spray paint the gondola in black
- Install latch for the gondola

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Najwa |
| Process monitor | Tikah |
| Recorder | Lee | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Using putty spray instead of putty filler since it is more convenient and faster
- Using black spray paint

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- The putty spray will make the surface smoother

**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- Putty spray will fill up the gaps between the wood cracks and thus, make it smoother
- Spray painting after using the putty spray will make the finished product look nicer

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Achieved an aesthetically pleasing gondola and a smoother gondola surface
- The finished product looks like:

![spray_painted_gondola](/uploads/5fcdd248fdb3b842b878ec71c3078056/spray_painted_gondola.jpeg)

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Try to attach the gondola to the airship belly
- Do a checklist for under the gondola team for the flight test in Week 14
