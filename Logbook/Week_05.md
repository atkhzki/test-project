## Engineering Logbook Week 05

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 26 November 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- After discussing with En. Azizi and En. Fiqri, our gondola tray design needs to be changed to another type of mechanism that is easier to access
- Maintenance for the control system group would be hard since the wire will get caught on the gondola while pulling the tray in/out
- This week agenda is to think about how to overcome that problem
- Also, the material for the gondola is also discussed

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Thebban |
| Process monitor | Syahirah |
| Recorder | Najwa | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Decided to do a design that can stack the 3 layers (top-carrier board, middle-esc, bottom-battery) on top of each other and securing it with a latch
- The design will look like a shoe rack, the designs were drawn using SOLIDWORKS
- The material that we are going to use are mppf and 3D printing filament

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- So that it is easier to remove the stacks when the control system needs to change any components on the carrier board
- Mppf is light yet durable, therefore it'll be great to be used for the gondola outer layer while the structural part, we are using 3D filament


**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- The new design as seen below, will be completely filled with mppf therefore, from an outside point of view, the gondola would just look like a solid box

![photo_2021-12-03_14-23-32](/uploads/ae3672c6987971436739949523d3237e/photo_2021-12-03_14-23-32.jpg)

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Easier access for changing any of the carrier board components
- Aesthetically pleasing design for the gondola

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Thinking on how to attach the payload system to the gondola
