## Engineering Logbook Week 07

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 10 December 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Met up with Fiqri at Lab H2.1(IDP Lab) on Wednesday to discuss more and finalized our gondola design
- Go survey the clamp for the payload attachment to the gondola

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Najwa |
| Process monitor | Atikah |
| Recorder | Lee | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- We have made the bottom part of the gondola to have a semi-hexagon shape for it to look like a product
- Also, added solid walls with vents instead of using mppf to cover up the hollow parts
- Bought a suitable clamp that can fit the 20mm diameter pvc pipe that payload team is using for the attachment

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- It looks more like a product and the design is cleaner
- Since the clamp is needed and we need to estimate the diameters of the holes for the nut and bolt to go in in order to secure the clamp

**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- Because the clamp nut and bolt hole needs to be accurate so that when we 3D print it later, the nut and bolt will fit perfectly without it being too tight/loose


**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Clean design and finalizing the design
- Can continue to progress to the next part, which is 3D printing it

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- 3D printing and assembling the gondola with all of the components
