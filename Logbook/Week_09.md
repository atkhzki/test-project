## Engineering Logbook Week 09

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 23 December 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Install the latch on every side of the gondola
- Touch up any parts that does not have enough glue

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Lee |
| Process monitor | Najme |
| Recorder | Thebban | 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Decide on how to make a hole on the gondola without damaging it

**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- The holes needed to screw on the latch was drilled using a power drill

**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- It is easier to drill rather than making a hole using the laser cutting since we can mark the hole position accurately using power drill

**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- The gondola is assembled together with the latch and it is as per the image below:

![gondola_assembled__1_](/uploads/20238797becfbc471655f1463750f6c8/gondola_assembled__1_.jpg)
- The latch holds the top and bottom layer of the gondola really well even after shaking it up 

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Assemble the latch on each side of the gondola and put all of the control system component in the gondola to test it out.
