## Engineering Logbook Week 04

| Item | Detail |
| ------ | ------ |
| Name | Nuratikah Binti Abu Zaki |
| Matrics No | 198785 |
| Subsystem | Design/Structure |
| Group | 5 |
| Date | 19 November 2021 |


**1. What is our agenda this week and what are our/my goals?**

**Team: Design/Structure(subsytem)**
- Met up with Fiqri at Lab H2.1(IDP Lab) on Wednesday to discuss on our proposed idea
- Also, we checked the past designs of the gondola and try to learn the designs' strengths and weaknesses
- We showed our sketch of the initial idea and Fiqri even gave some of his suggestions too
- We also discussed on which material that is suitable for the gondola
- We presented the gondola sketch on Friday's meeting and there are some improvements needed

**Team: Group 5**
- The same thing as last week except this week the management roles have rotated:

| Roles | Name |
| ------ | ------ |
| Coordinator | Adlina |
| Checker | Najme |
| Process monitor | Thebban |
| Recorder | Syahirah| 


**2. What discussion did you/your team make in solving a problem?**

**Team: Design/Structure (subsytem)**
- Addressing the problem regarding the gondola which was weight and also the tray design 
- We discuss on how the tray design can be a problem for the carrier board in terms of maintenance


**3. How did you/your team make those decisions (method)**

**Team: Design/Structure(subsytem)**
- We discussed on why the tray design can cause a problem and we started to brainstorm a new idea for the gondola design that can implements 3 different layers on top of each other 


**4. Why did you/your team make that choice (justification) when solving the problem?**

**Team: Design/Structure(subsytem)**
- Since when it is sliding in and out, the wires will be caught in the lightening holes therefore, we have decided to not go through with the tray design for the gondola layers


**5. What was the impact on you/your team/the project when you make that decision?**

**Team: Design/Structure(subsytem)**
- Overcome the problem of the carrier board wires being hard to maintain when it is sliding in/out

**6. What is the next step?**

**Team: Design/Structure(subsytem)**
- Come up with a new design that can stack up 3 layers for the gondola
